#ifndef LIST
#define LIST

template <class T>
class Node {
public:
	T data;
	Node<T> *next, *prev;
	Node<T>()
	{
		next = prev = 0;
	}
	Node<T>(T el, Node<T> *n = 0, Node<T> *p = 0)
	{
		data = el; next = n; prev = p;
	}
};



template <class T>
class List {

public:

	List() { head = tail = 0; }
	int isEmpty() { return head == 0; }


	~List() {
		for (Node<T> *p; !isEmpty(); ) {
			p = head->next;
			delete head;
			head = p;
		}
	}

	void pushToHead(T el)
	{
		head = new Node<T>(el, head, 0);
		if (tail == 0)
		{
			tail = head;
		}
		else
		{
			head->next->prev = head;
		}
	}
	void pushToTail(T el)
	{
		Node<T> *tmp = new Node<T>(el, 0, tail); //create new node
		if (tail == 0) //if no tail
		{
			head = tail = tmp; //head and tail is the same node
		}
		else
		{
			tail->next = tmp; //pu tmp after tail
			tail = tmp; //move tail to tmp. make it as new tail
		}
	}

	T popHead()
	{
		T el = head->data;
		Node<T> *tmp = head;
		if (head == tail)
		{
			head = tail = 0;
		}
		else
		{
			head = head->next;
			head->prev = 0;
		}
		delete tmp;
		return el;
	}

	T popTail()
	{
		T el = tail->data;
		Node<T> *tmp = head;
		Node<T> *tailNode = tail;

		if (isEmpty())//checking that the list is empty or not
		{
			return NULL;
		}
		else if (head == tail) //if there is only one node in the list
		{
			head = tail = 0;
		}
		else
		{
			while (tmp->next != tail) //if next to tmp not equal to tail
			{
				tmp = tmp->next; // move next
			}
			tail = tmp; //move tail back
			tail->next = NULL; //make tail point to null
			return el; //return data that it was deleted
		}

	}


	bool search(T el)
	{
		Node<T> *tmp = head;
		if (isEmpty())
		{
			return false; //it still not found if it no node in list
		}
		while (tmp != NULL) //if we have at least 1 in list
		{
			if (tmp->data != el) //if tmp not equal to data that we search
			{
				tmp = tmp->next; //move to next node
			}
			else
			{
				return true; //if it found return true
			}
		}
		return false; //out of list 
	}

	void print()
	{
		using namespace std;
		if (head == tail)
		{
			cout << head->data;
		}
		else
		{
			Node<T> *tmp = head;
			while (tmp != tail)
			{
				cout << tmp->data;
				tmp = tmp->next;
			}
			cout << tmp->data;
		}
	}

private:
	Node<T> *head, *tail;
};

#endif