#include <iostream>
#include "list.h"
using namespace std;

int main()
{
	//test char
	List<char> mylist;
	mylist.pushToHead('k');
	mylist.pushToTail('e');
	mylist.pushToTail('n');
	mylist.pushToTail('n');
	mylist.popTail();
	mylist.print();

	cout<<endl<< "Searching for 'e'"<<endl;
	if (mylist.search('e')) {
		cout << "e found !!" <<endl;
	}
	else {
		cout << "e not found !!"<<endl;
	}

	cout << endl << "Searching for 'z'" << endl;
	if (mylist.search('z')) {
		cout << "z found !!" << endl;
	}
	else {
		cout << "z not found !!" << endl;
	}
	//test int
	List<int> myNumlist;
	myNumlist.pushToHead(1);
	myNumlist.pushToTail(2);
	myNumlist.pushToTail(3);
	myNumlist.pushToTail(4);
	myNumlist.popTail();
	myNumlist.print();
	system("pause");
	//TO DO! Complete the functions, then write a program to test them.
	//Adapt your code so that your list can store data other than characters - how about a template?
}